package com.regino.springnonreactive.model;

import com.regino.springnonreactive.data.UserAccept;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.redis.core.RedisHash;
import java.io.Serializable;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@RedisHash("users")
public class User implements Serializable {
    private String id;
    private String name;
    private String email;

    public User(String name, String email) {
        this.id = UUID.randomUUID().toString();
        this.email = email;
        this.name = name;
    }

    public User editInfo(UserAccept user) {
        this.name = user.getName();
        this.email = user.getEmail();
        return this;
    }
}
