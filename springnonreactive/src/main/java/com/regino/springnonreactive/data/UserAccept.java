package com.regino.springnonreactive.data;

public class UserAccept {
    private final String name;
    private final String email;

    public UserAccept(String name, String email) {
        this.name = name;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }
}

