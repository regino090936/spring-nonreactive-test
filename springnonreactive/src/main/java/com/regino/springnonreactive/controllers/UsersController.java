package com.regino.springnonreactive.controllers;

import com.regino.springnonreactive.data.UserAccept;
import com.regino.springnonreactive.model.User;
import com.regino.springnonreactive.repos.interfaces.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/users")
public class UsersController {
    private final UserRepository userRepository;

    @Autowired
    public UsersController( UserRepository userRepositoryImplementation) {
        userRepositoryImplementation.initialize();
        this.userRepository = userRepositoryImplementation;
    }

    @GetMapping
    public Iterable<User> readUsers() {
        return userRepository.getUsers();
    }

    @GetMapping("/{id}")
    public ResponseEntity<User> readUser(@PathVariable String id) {
        User user = userRepository.getUser(id);
        if (user != null) {
            return ResponseEntity
                    .ok(user);
        } else {
            return ResponseEntity
                    .notFound()
                    .build();
        }
    }

    @PostMapping
    public ResponseEntity<Void> createUser(@RequestBody UserAccept user) {
        if (userRepository.addUser(user.getName(), user.getEmail())) {
            return ResponseEntity
                    .ok()
                    .build();
        } else {
            return ResponseEntity
                    .badRequest()
                    .build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteUser(@PathVariable String id) {
        if (userRepository.deleteUser(id)) {
            return ResponseEntity
                    .ok()
                    .build();
        } else {
            return ResponseEntity
                    .notFound()
                    .build();
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Void> editUser(@PathVariable("id") String id, @RequestBody UserAccept user) {
        if (userRepository.editUser(id, user)) {
            return ResponseEntity
                    .ok()
                    .build();
        } else {
            return ResponseEntity
                    .badRequest()
                    .build();
        }
    }
}
