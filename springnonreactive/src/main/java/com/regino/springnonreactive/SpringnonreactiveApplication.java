package com.regino.springnonreactive;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

@SpringBootApplication
public class SpringnonreactiveApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringnonreactiveApplication.class, args);
    }

}
