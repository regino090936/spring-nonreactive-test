package com.regino.springnonreactive.repos.implementations.postgres.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

@Configuration
@ConditionalOnProperty(prefix = "datasource", name = "implementation", havingValue = "postgres", matchIfMissing = true)
public class PostgreSqlConfig {

    @Value("${database.url}")
    private String url;

    @Value("${database.username}")
    private String name;

    @Value("${database.password}")
    private String password;

    @Bean
    public Statement getStatement() throws ClassNotFoundException {
        Statement statement = null;
        try {
            Connection connection = DriverManager.getConnection(url, name, password);
            statement = connection.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
            System.exit(2);
        }
        return statement;
    }
}
