package com.regino.springnonreactive.repos.implementations.redis.dao;

import com.regino.springnonreactive.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserDao extends CrudRepository<User, String> {
}
