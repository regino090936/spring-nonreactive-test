package com.regino.springnonreactive.repos.interfaces;

import com.regino.springnonreactive.data.UserAccept;
import com.regino.springnonreactive.model.User;

public interface UserRepository {
    void initialize();

    Iterable<User> getUsers();

    User getUser(String id);

    boolean addUser(String name, String email);

    boolean deleteUser(String id);

    boolean editUser(String id, UserAccept user);
}
