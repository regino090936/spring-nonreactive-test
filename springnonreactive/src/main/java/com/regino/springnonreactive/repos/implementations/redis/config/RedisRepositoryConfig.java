package com.regino.springnonreactive.repos.implementations.redis.config;

import com.regino.springnonreactive.repos.implementations.redis.dao.UserDao;
import com.regino.springnonreactive.repos.implementations.redis.repos.UserRepositoryRedis;
import com.regino.springnonreactive.repos.interfaces.UserRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnProperty(prefix = "datasource", name = "implementation", havingValue = "redis")
public class RedisRepositoryConfig {

    @Bean
    UserRepository userRepositoryRedis(UserDao userDao, @Value("${datasource.initial-size}") int initialSize) {
       return new UserRepositoryRedis(userDao, initialSize);
    }
}
