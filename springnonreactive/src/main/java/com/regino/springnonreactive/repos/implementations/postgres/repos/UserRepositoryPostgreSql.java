package com.regino.springnonreactive.repos.implementations.postgres.repos;

import com.regino.springnonreactive.data.UserAccept;
import com.regino.springnonreactive.model.User;
import com.regino.springnonreactive.repos.interfaces.UserRepository;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;
import java.util.stream.Stream;

public class UserRepositoryPostgreSql implements UserRepository {

    private final int initialSize;
    private final Statement statement;

    public UserRepositoryPostgreSql(int initialSize, Statement statement) {
        this.initialSize = initialSize;
        this.statement = statement;
    }

    public void initialize(){
        try {
            statement.executeUpdate("drop table if exists user_table;");
            statement.executeUpdate(
                        "CREATE TABLE IF NOT EXISTS user_table\n" +
                        "(\n" +
                        "    id VARCHAR(50) PRIMARY KEY,\n" +
                        "    name VARCHAR(30) NOT NULL,\n" +
                        "    email VARCHAR(40) NOT NULL \n" +
                        ");"
            );
            statement.executeUpdate("delete from user_table;");
            Stream.iterate(1, i -> i + 1)
                    .limit(initialSize)
                    .forEach(i -> addUser("mysql_name" + i, "email" + i));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Set<User> getUsers() {
        Set<User> users = new HashSet<>();
        try {
            ResultSet resultSet = statement.executeQuery("SELECT * FROM user_table;");
            while (resultSet.next()) {
                users.add(new User(
                        resultSet.getString(1),
                        resultSet.getString(2),
                        resultSet.getString(3)
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            System.exit(3);
        }
        return users;
    }

    public User getUser(String id) {
        ResultSet result = null;
        final String executeTemplate = "select * from user_table where id = '%s';";
        try {
            result = statement.executeQuery(String.format(executeTemplate, id));
            if (result.next()) {
                return new User(
                        result.getString(1),
                        result.getString(2),
                        result.getString(3)
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean addUser(String name, String email) {
        if (name.equals("") || email.equals("")) {
            return false;
        }
        final String executeTemplate = "insert into user_table(id, name, email) VALUES('%s', '%s', '%s');";
        try {
            statement.executeUpdate(String.format(executeTemplate, UUID.randomUUID().toString(), name, email));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    public boolean deleteUser(String id) {
        if (getUser(id) == null) {
            return false;
        }
        final String executeTemplate = "DELETE FROM user_table WHERE id='%s';";
        try {
            statement.executeUpdate(String.format(executeTemplate, id));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    public boolean editUser(String id, UserAccept user) {
        if (getUser(id) == null || user.getName().equals("") || user.getEmail().equals("")) {
            return false;
        }
        final String executeTemplate = "UPDATE user_table SET name='%s', email='%s' WHERE id='%s';";
        try {
            statement.executeUpdate(String.format(executeTemplate, user.getName(), user.getEmail(), id));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }
}
