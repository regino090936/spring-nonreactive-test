package com.regino.springnonreactive.repos.implementations.redis.repos;

import com.regino.springnonreactive.data.UserAccept;
import com.regino.springnonreactive.model.User;
import com.regino.springnonreactive.repos.implementations.redis.dao.UserDao;
import com.regino.springnonreactive.repos.interfaces.UserRepository;
import java.util.stream.Stream;

public class UserRepositoryRedis implements UserRepository {
    private final UserDao dao;
    private final int initialSize;

    public UserRepositoryRedis(UserDao dao, int initialSize) {
        this.dao = dao;
        this.initialSize = initialSize;
    }

    @Override
    public void initialize(){
        dao.deleteAll();
        Stream.iterate(1, i -> i + 1)
                .limit(initialSize)
                .forEach(i -> dao.save(new User("redis_name" + i, "email" + i)));
    }

    @Override
    public Iterable<User> getUsers() {
        return dao.findAll();
    }

    @Override
    public User getUser(String id) {
        return dao.findById(id).orElse(null);
    }

    @Override
    public boolean addUser(String name, String email) {
        if (name.equals("") || email.equals("")) {
            return false;
        }
        dao.save(new User(name, email));
        return true;
    }

    @Override
    public boolean deleteUser(String id) {
        if (dao.findById(id).isEmpty()) {
            return false;
        }
        dao.deleteById(id);
        return true;
    }

    @Override
    public boolean editUser(String id, UserAccept user) {
        if (user.getName().equals("") || user.getEmail().equals("") || dao.findById(id).isEmpty()) {
            return false;
        }
        dao.save(
                dao.findById(id)
                        .orElseThrow()
                        .editInfo(user)
        );
        return true;
    }
}
