package com.regino.springnonreactive.repos.implementations.mysql.config;

import com.regino.springnonreactive.repos.implementations.postgres.repos.UserRepositoryPostgreSql;
import com.regino.springnonreactive.repos.interfaces.UserRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import java.sql.Statement;

@Configuration
@ConditionalOnProperty(prefix = "datasource", name = "implementation", havingValue = "postgres", matchIfMissing = true)
public class PostgreSqlRepositoryConfig {

    @Bean
    UserRepository userRepositoryPostgreSql(Statement statement, @Value("${datasource.initial-size}") int initialSize) {
        return new UserRepositoryPostgreSql(initialSize, statement);
    }
}
