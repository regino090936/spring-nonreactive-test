package com.regino;

import java.io.IOException;
import java.net.ConnectException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.zip.DataFormatException;

import com.google.gson.*;
import com.regino.data.User;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws IOException, InterruptedException {
        final Gson gson = new Gson();
        final HttpClient client = HttpClient.newHttpClient();
        HttpRequest httpRequest = null;
        final String pattern = "http://localhost:8080/api/users/%s";
        String jsonStr;
        User user;

        try {
            switch (args[0].toLowerCase()) {
                case "post":
                    user = new User(args[1], args[2]);
                    jsonStr = gson.toJson(user, User.class);
                    httpRequest = HttpRequest.newBuilder()
                            .header("Content-Type", "application/json")
                            .POST(HttpRequest.BodyPublishers.ofString(jsonStr))
                            .uri(URI.create(String.format(pattern, "")))
                            .build();
                    break;
                case "delete":
                    httpRequest = HttpRequest.newBuilder()
                            .header("Content-Type", "application/json")
                            .DELETE()
                            .uri(URI.create(String.format(pattern, args[1])))
                            .build();
                    break;
                case "patch":
                case "put":
                    user = new User(args[2], args[3]);
                    jsonStr = gson.toJson(user, User.class);
                    httpRequest = HttpRequest.newBuilder()
                            .header("Content-Type", "application/json")
                            .PUT(HttpRequest.BodyPublishers.ofString(jsonStr))
                            .uri(URI.create(String.format(pattern, args[1])))
                            .build();
                    break;
                default:
                    throw new DataFormatException();
            }

            client.send(httpRequest, HttpResponse.BodyHandlers.ofString());

        } catch (DataFormatException e) {
            System.out.println("Error: wrong request method");
            System.exit(1);
        } catch (ConnectException e) {
            System.out.println("Error: cannot connect to the server");
            System.exit(1);
        } catch (IndexOutOfBoundsException e) {
            System.out.println("Error: wrong parameters");
            System.exit(1);
        }
    }
}
