## SERVER
run 
```
mvn spring-boot:run
```

## CLIENT
build
```
mvn clean package assembly:single
```

run
```
java -jar target/springnonreactiveclient-1.0-SNAPSHOT-jar-with-dependencies.jar [requst method] [id(optionaly)] [name(optionally)] [email(optionaly)]

